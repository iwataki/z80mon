  .AREA _HEADER (ABS)

  SYSCENT .EQU 5
  NPUTSTR  .EQU 4;SYSCALL 4
  .ORG 0x9000
  JP MAIN
TESTMSG:
  .ASCII "This is test program.\r\n"
  .ASCII "Can you read this message?\r\n"
  .DB 0
MAIN:
  LD HL,#TESTMSG
  LD C,#NPUTSTR
  CALL SYSCENT
  RET
